<!DOCTYPE html>
<html>
<head>
    <title>Schweppes Sip & Win Promotion</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Sea King Restaurant Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- //for-mobile-apps -->
    <link href="styles/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="styles/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- js -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <!-- //js -->
    <!-- login-pop-up -->
    <script src="js/menu_jquery.js"></script>
    <!-- //login-pop-up -->
    <!-- animation-effect -->
    <link href="styles/animate.min.css" rel="stylesheet">
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!-- //animation-effect -->    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css' />
    <script>
        $(document).ready(function () {


            $(document).on('submit', '#myform', function (e) {

                //e.preventDefault();
                var data = $(this).serialize();
                
                //hide error message
                $("#results").hide();

                $.ajax({

                    type: 'POST',
                    url: 'register.php',
                    data: data,
                    success: function (data) {
                        console.log(data);
                        $("#results").fadeIn("slow", function () {
                            $("#results").html(data);
                        });

                    }
                });
                return false;
            });
        });
    </script>

    <!-- //animation-effect -->
    <link
        href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
        rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
</head>

<body>
<!--facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- header -->
<div class="header-top ">
    <div class="container">
        <div class="schweppes-pos-2 animated wow slideInLeft" data-wow-delay=".5s">

            <ul class="schweppes-list">     </ul>
        </div>
        <div class="schweppes-pos animated wow slideInLeft schweppes-head-bg" data-wow-delay=".7s">
            <ul class="schweppes-list">   <li><a href="#"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a></li>        <li><a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a></li>        <li><a href="#"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a></li>      </ul>
        </div>
        <!--  <div class="header-top-right">
              <div id="loginContainer login"><a href="#" id="loginButton"></a>

              </div>
          </div>-->

    </div>
</div>
<div class="header-nav ">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo animated wow slideInLeft" data-wow-delay=".5s">
                    <a class="navbar-brand" href="index.php"><img src="images/schweppeslogo.png"> </a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="results.php">Results</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->

        </nav>
    </div>
</div>
<!-- //header -->
<!-- banner -->
<div class="banner s-padding">
    <div class="banner-pos banner1">
        <div class="container">
            <div class="custom-info animated wow slideInUp" data-wow-delay=".5s">
                <!--<h2>
                    Schweppes <span>Zimbabwe Limited</span>
                </h2>
                <p>Amazing And delicious dishes only with Us !</p>-->
            </div>
        </div>

        <div class="container col-sm-12 ">
            <div class="banner-posit animated wow zoomIn" data-wow-delay=".5s">
                <h2>Register Now To Enter Promotion</h2><br/>

                <div class="alert alert-danger" id="results" style="display:none;"></div>

                <div class="row">


                    <form id="myform" method="post">
                        <div class="form-group row">

                            <div class="col-sm-5">
                                <input type="text" class="form-control form-control-lg" name="name" placeholder="Name">
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control form-control-lg" name="surname"
                                       placeholder="Surname">
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control form-control-lg" name="age"/>
                                <option>Age</option>
                                <?php
                                for ($i = 1; $i <= 100; $i++) {
                                    echo '<option value="' . $i . '">'
                                        . $i
                                        . '</option>';
                                }
                                ?>


                                </select>

                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-sm-6">
                                <input type="email" name="email" class="form-control form-control-lg"
                                       placeholder="you@example.com">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-lg" name="phone"
                                       placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-sm-6">
                                <textarea class="form-control" rows="4" name="address"
                                          placeholder="Enter Address"></textarea>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-lg" name="city"
                                       placeholder="City"><br/>
                                <input type="text" class="form-control form-control-lg" name="suburb"
                                       placeholder="Suburb">
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-sm-6">
                                <select id="country" name="flavour" class="form-control form-control-lg">
                                    <option value="">Choose Flavour Bought</option>
                                    <option value="orangecrush">Orange Crush</option>
                                    <option value="blackberry">Blackberry</option>
                                    <option value="creamsoda">Cream Soda</option>
                                    <option value="raspberry">Raspberry</option>
                                </select>
                            </div>
                            <div class="col-sm-6">

                                <input type="text" class="form-control form-control-lg" name="code"
                                       placeholder="Unique Code">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="checkbox" name="terms" value="terms"> Accept Terms and conditions
                            </div>

                        </div>

                        <div class="form-group row">

                            <div class="col-sm-2">

                            </div>
                            <div class="col-sm-8">

                                <input type="submit" class="form-control form-control-lg s-button" name="submit"
                                       placeholder="Submit">
                            </div>

                            <div class="col-sm-2">

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
</div>

<!---strat-date-piker---->
<link rel="stylesheet" href="styles/jquery-ui.css"/>
<script src="js/jquery-ui.js"></script>
<script>
    $(function () {
        $("#datepicker,#datepicker1").datepicker();
    });
</script>
<!---/End-date-piker---->

<!-- //banner -->
<!-- banner-bottom -->
<div class="banner-bottom">
    <div class="container">
        <div class="banner-bottom-grids">
            <div class="col-md-4 banner-bottom-grid animated wow slideInLeft s-shadow" data-wow-delay=".5s">
                <h3>Prices to be Won</h3>
                <ol class="s-list">
                    <li><p>Cash Prices plus Hamper</p></li>
                    <li><p>Cash Prices</p></li>
                    <li><p>Hamper</p></li>
                </ol>
            </div>
            <div class="col-md-4 banner-bottom-grid animated wow slideInLeft s-shadow" data-wow-delay=".7s">
                <!--<h3>How To Enter Promotion</h3>-->
                <a class="twitter-timeline" data-width="300" data-height="500" href="https://twitter.com/rubiemgroup">Tweets by rubiemgroup</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

            </div>
            <div class="col-md-4 banner-bottom-grid animated wow slideInLeft s-shadow" data-wow-delay=".8s">
                <!--facebook-->
                <div class="fb-page" data-href="https://www.facebook.com/Schweppes-Zimbabwe-Limited-239101002789383/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Schweppes-Zimbabwe-Limited-239101002789383/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Schweppes-Zimbabwe-Limited-239101002789383/">Schweppes Zimbabwe Limited</a></blockquote></div>
                <!--endFbk-->


                <!--<div class="banner-bottom-grid-left">
                    <img src="images/orangecrush.png" alt=" " class="img-responsive"/>
                </div>
                <div class="banner-bottom-grid-left">
                    <img src="images/creamsoda.png" alt=" " class="img-responsive"/>
                </div>
                <div class="clearfix"></div>
                <div class="banner-bottom-grid-left">
                    <img src="images/raspberry.png" alt=" " class="img-responsive"/>
                </div>
                <div class="banner-bottom-grid-left">
                    <p>Just Buy One!</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>-->
            </div>
        </div>
    </div>
    <!-- //banner-bottom -->
    <!-- footer -->

    <div class="footer-copy">
        <div class="container">
            <div class="footer-left animated wow slideInUp" data-wow-delay=".5s">
                <p>© 2016 Schweppes. All rights reserved | Powered by <a href="http://www.rubiem.com" target="_blank">Rubiem
                        Innovations</a>
                </p>
            </div>
            <div class="footer-right animated wow slideInUp" data-wow-delay=".7s">
                <div class="schweppes-pos animated wow slideInLeft schweppes-head-bg" data-wow-delay=".7s">
                    <ul class="schweppes-list">   <li><a href="#"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a></li>        <li><a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a></li>        <li><a href="#"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a></li>      </ul>
                </div>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //events -->
<!-- footer -->

    <!-- //footer -->
    <!-- for bootstrap working -->
    <script src="js/bootstrap.js"></script>
    <!-- //for bootstrap working -->
</body>

</html>
