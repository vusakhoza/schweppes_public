<?php
/* Database connection start */
/*$servername = "localhost";
$username = "root";
$password = "";
$dbname = "datatable";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());*/

include("connection.php");

/* Database connection end */


// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;


$columns = array(
// datatable column index  => database column name
    0 =>'name',
    1 =>'email',
    2 => 'phone',
    3 => 'address',
    4 => 'suburb',
    5 => 'city',
    6 => 'age',
    7 => 'flavour',
    8 => 'code',
    9 => 'upload_date',
    10 => 'status'
);

// getting total number records without any search
$sql = "SELECT name, email, phone, address, suburb, city, age, flavour, code, upload_date, status";
$sql.=" FROM participants";
$query=mysqli_query($con, $sql) or die("get-all-participants.php: get employees");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


$sql = "SELECT name, email, phone, address, suburb, city, age, flavour, code, upload_date, status";
$sql.=" FROM participants WHERE 1=1";
if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
    $sql.=" AND ( name LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR phone LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR email LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR address LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR city LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR suburb LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR status LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR flavour LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR upload_date LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR code LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR age LIKE '".$requestData['search']['value']."%' )";
}
$query=mysqli_query($con, $sql) or die("get-all-participants.php: get employees");
$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */
$query=mysqli_query($con, $sql) or die("get-all-participants.php: get employees");

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();

    $nestedData[] = $row["name"];
    $nestedData[] = $row["email"];
    $nestedData[] = $row["phone"];
    $nestedData[] = $row["address"];
    $nestedData[] = $row["city"];
    $nestedData[] = $row["suburb"];
    $nestedData[] = $row["age"];
    $nestedData[] = $row["flavour"];
    $nestedData[] = $row["code"];
    $nestedData[] = $row["upload_date"];
    $nestedData[] = $row["status"];

    $data[] = $row;
}



$json_data = array(
    "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    "recordsTotal"    => intval( $totalData ),  // total number of records
    "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
    "data"            => $data   // total data array
);

echo json_encode($json_data);  // send data as json format

?>
