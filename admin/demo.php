<?php
include('connection.php');
if (isset($_POST['submit'])) {


    if (mysqli_connect_errno()) {
        echo mysqli_connect_errno();
        exit();
    }
    $check_draw = "SELECT * FROM draws WHERE draw_date = CURDATE()";
    $cd = mysqli_query($con, $check_draw);

    if (mysqli_num_rows($cd) > 0) {
        draw();
    } else {
        $create_draw = "INSERT INTO draws (draw_date) VALUES (CURDATE())";

        $new_draw = mysqli_query($con, $create_draw);

        if ($new_draw) {
            draw();
        }
    }


}
function draw()
{
    $conn = mysqli_connect("localhost", "root", "", "schweppes") or die('Unable to Connect');

    $detailed = array(); //full surveyor account details, i.e. name and telephone
    $fulllist = array(); //resulting list containing the unique keys retrieved by the SQL statement
    $winners = array(); //final winners array
    $backup = array(); //backup winners array

    $winnerslength = 5; //the number of winners we desire
    $backuplength = round(($winnerslength / 2), 0, PHP_ROUND_HALF_UP); //number of backup winners, in this case 5

    //example SQL to give us a list of surveyors. Normally you would at this point add your filter requirements, like date range, data filters, etc in the WHERE clause.
    $tempwinners = mysqli_query($conn, "SELECT DISTINCT * FROM participants WHERE status = '0'");

//populate both the keyed array containing all the surveyor details, as well as our workhorse array which contains only the surveyor IDs
    while ($tempwinner = mysqli_fetch_assoc($tempwinners)) {
        $detailed[$tempwinner['id']] = $tempwinner;
        $fulllist[] = $tempwinner['id'];
    }

//one last check to make sure we are unique
    array_unique($fulllist);

//shuffle the winners
    shuffle($fulllist);

//if the winners pool is smaller or equal to the desired winners count, use everything
    if (count($fulllist) <= $winnerslength) {
        foreach ($detailed as $detailedrow) {
            $winners[] = $detailedrow;
        }
    } else {
//slice off our winners and backup winners from the shuffled list
        $temps = array_slice($fulllist, 0, ($winnerslength + $backuplength));
        $runner = 0;
//populate the arrays
        foreach ($temps as $detailedrow) {
            if ($runner < $winnerslength) {
                $winners[] = $detailed[$detailedrow];
                $runner = $runner + 1;
            } else {
                $backup[] = $detailed[$detailedrow];
            }
        }
    }

    foreach ($fulllist as $value) {

        $i = $value[0];

        $sql_update = "UPDATE participants SET status = '3' WHERE id='$i'";

        $r = mysqli_query($conn, $sql_update);

    }

//and voila, you now have a randomly drawn winners and backup winners array!

    foreach ($winners as $value) {
        $n = $value["name"];
        $p = $value["phone"];
        $e = $value["email"];

        $sql_winner = "INSERT INTO winners(name, phone, draw_date) VALUES ('$n', '$p', CURDATE())";

        $r = mysqli_query($conn, $sql_winner);

        $to = $e;
        $subject = "Schweppes. Congratulations";

        $message = "Dear " . $n . " you have been chosen as a winner in the Mazoe Sip into the cash
promotion";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <mail-noreply@schweppes.co.zw>' . "\r\n";

        mail($to, $subject, $message, $headers);

        //echo 'The Winner is ' . $value["name"] . '<br/>';
    }


}

?>

<html>
<head>
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all">

</head>
<body>
<form method="post">
    <input class="button" id="button" type="submit" name="submit" value="submit"/>
</form>

</body>
</html>