<?php
session_start();//session starts here

if (isset($_SESSION['username'])) {

    header("Location:index.php");
    exit;
}
?>
<!DOCTYPE HTML>
<html>
<head>

    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <link href="../css/login.css" rel="stylesheet" type="text/css" media="all">
    <!--js-->
    <script src="../js/jquery.js"></script>
    <!--icons-css-->
    <link href="../css/font-awesome.css" rel="stylesheet">
    <!--Google Fonts-->
    <link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
    <!--static chart-->
</head>
<body>
<div class="login-page">
    <?php

    include("connection.php");

    if (isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $check_user = "SELECT * FROM users WHERE username='$username' AND password='$password'";

        $run = mysqli_query($con, $check_user);

        if (mysqli_num_rows($run) > 0) {

            echo "<script>window.open('index.php','_self')</script>";

            $_SESSION['username'] = $username;//here session is used and value of $username store in $_SESSION.
        } else {
            //echo "<script>alert('Username or Password is incorrect!')</script>";
            echo '<div class="alert alert-danger alert-dismissable">
									<button aria-hidden="true" data-dismiss="alert" class="close" type="button"> × </button>
									<strong>Username or Password is incorrect!</strong> </div>';
        }
    }
    ?>
    <div class="login-main">

        <div class="login-head">
            <h1>Login</h1>
        </div>
        <div class="login-block">

            <form role="form" method="post" action="login.php">
                <input type="text" name="username" placeholder="Username" required="">
                <input type="password" name="password" class="lock" placeholder="Password" required="">
                <input type="submit" name="login" value="Login">
            </form>
        </div>
    </div>
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
    <p></p>
</div>
<!--COPY rights end here-->

<!--scrolling js-->
<script src="../js/jquery.nicescroll.js"></script>
<script src="../js/scripts.js"></script>
<!--//scrolling js-->
<script src="../js/bootstrap.js"></script>
<!-- mother grid end here-->
</body>
</html>

