<?php
//include connection file
include_once("connection.php");

// initilize all variable
$params = $columns = $totalRecords = $data = array();

$params = $_REQUEST;

//define index of column
$columns = array(
    0 =>'name',
    1 =>'email',
    2 => 'phone',
    3 => 'address',
    4 => 'suburb',
    5 => 'city',
    6 => 'age',
    7 => 'flavour',
    8 => 'code',
    9 => 'upload_date',
    10 => 'status'
);

$where = $sqlTot = $sqlRec = "";

// getting total number records without any search
$sql = "SELECT name, email, phone, address, suburb, city, age, flavour, code, upload_date, status FROM `participants` ";
$sqlTot .= $sql;
$sqlRec .= $sql;


$sqlRec .=  " ORDER BY name";

$queryTot = mysqli_query($con, $sqlTot) or die("database error:". mysqli_error($conn));


$totalRecords = mysqli_num_rows($queryTot);

$queryRecords = mysqli_query($con, $sqlRec) or die("error to fetch employees data");

//iterate on results row and create new index array of data
while( $row = mysqli_fetch_row($queryRecords) ) {
    $data[] = $row;
}

$json_data = array(
    "draw"            => 1,
    "recordsTotal"    => intval( $totalRecords ),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $data   // total data array
);

echo json_encode($json_data);  // send data as json format
?>