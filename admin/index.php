<?php
session_start();

if(!isset($_SESSION['username'])){
    header("Location: login.php");//redirect to login page to secure the welcome page without login access.
}

include_once 'connection.php';

if (isset($_POST['submit'])) {
    if ($_FILES['csv_data']['name']) {
        $arrFileName = explode('.', $_FILES['csv_data']['name']);
        if ($arrFileName[1] == 'csv') {
            $handle = fopen($_FILES['csv_data']['tmp_name'], "r");
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $name = mysqli_real_escape_string($con, $data[0]);
                $age = mysqli_real_escape_string($con, $data[1]);
                $phone = mysqli_real_escape_string($con, $data[2]);
                $email = mysqli_real_escape_string($con, $data[3]);
                $address = mysqli_real_escape_string($con, $data[4]);
                $suburb = mysqli_real_escape_string($con, $data[5]);
                $city = mysqli_real_escape_string($con, $data[6]);
                $flavour = mysqli_real_escape_string($con, $data[7]);
                $code = mysqli_real_escape_string($con, $data[8]);
                $import = "INSERT into participants(phone,name, email, address, suburb, city, age, flavour, code, upload_date, status) values('$phone','$name', '$email', '$address', '$suburb', '$city', '$age', '$flavour', '$code', CURDATE(), '0')";
                $result = mysqli_query($con, $import);
            }
            fclose($handle);
            $message = "Import done.";

        }
    }
} else if (isset($_POST['delete'])) {
    $result = mysqli_query($con, 'TRUNCATE TABLE participants');

    if ($result) {
        echo "table has been truncated";
    } else echo "Something went wrong: " . mysqli_error($con);
}
?>


</form>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/logo-nav.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="../images/logo.png" alt="">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="view.php">View All Participants</a>
                </li>
                <li>
                    <a href="winners.php">View All Winners</a>
                </li>
                <!--<li>
                    <a href="draws.php">View All Draws</a>
                </li>-->

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="draw.php">Start Draw</a>
                </li>
                <li>
                    <a href="logout.php">Logout</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <?php
            if (isset($message)) {
                ?>
                <div class="alert alert-success" data-dismiss="alert" aria-label="Close">
                    <strong>Success!</strong> <?php echo $message; ?>
                </div>
                <?php
            }

            ?>
            <form method='POST' enctype='multipart/form-data'>
                <div class="form-group">
                    <label for="csv_data">Upload CSV</label>
                    <input type="file" class="form-control" id="csv_data" name="csv_data">
                </div>
                <input class="btn btn-default" type='submit' name='submit' value='import'/><br/>

            </form>
        </div>

        <div class="col-lg-6">
            <!--<h2>Empty All the participants in the database.</h2>
            <form method='POST' enctype='multipart/form-data'>
                <input class="btn btn-danger" type='submit' name='delete' value='Empty Database'/>
            </form>-->
            <!--<h2>Start a New Draw</h2>
            <a class="btn btn-danger" href="draw.php">Start</a>-->
        </div>
    </div>
</div>
<!-- /.container -->

<!-- jQuery -->
<script src="../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

</body>

</html>
