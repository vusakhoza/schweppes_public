<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
    <title>Schweppes | Draws</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Sea King Restaurant Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- //for-mobile-apps -->
    <link href="styles/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="styles/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- js -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <!-- //js -->
    <!-- login-pop-up -->
    <script src="js/menu_jquery.js"></script>
    <!-- //login-pop-up -->
    <!-- animation-effect -->
    <link href="styles/animate.min.css" rel="stylesheet">
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!-- //animation-effect -->
    <link
        href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
        rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
</head>

<body>
<!-- header -->
<div class="header-top">
    <div class="container">
        <div class="header-top-left animated wow slideInLeft" data-wow-delay=".5s">
            <p>Schweppes Sip & Win Promotion.</p>
        </div>
        <div class="header-top-left1 animated wow slideInLeft" data-wow-delay=".7s">
            <h1>Contact Us<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></h1>
        </div>
        <div class="header-top-right">
            <div id="loginContainer login">

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="header-nav">
    <div class="container">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo animated wow slideInLeft" data-wow-delay=".5s">
                    <a class="navbar-brand" href="index.php"><img src="images/logo.png"></a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="index.php">Home</a></li>
                    <li class="active"><a href="results.php">Results</a></li>
                </ul>

                <!--<div class="search">
                    <form>
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span><input type="email" class="text" value="Enter Your Text..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Text...';}" required="">
                    </form>
                </div>-->
            </div><!-- /.navbar-collapse -->

        </nav>
    </div>
</div>
<!-- //header -->
<!-- banner -->
<div class="banner2 animated wow slideInUp" data-wow-delay=".5s">
</div>
<!-- //banner -->
<!-- about -->
<div class="about">
    <div class="container">
        <div class="about-grids">

            <div class="col-md-6 about-grid animated wow slideInLeft" data-wow-delay=".5s">
                <h3>Draws</h3>
                <?php
                include("connection.php");
                $limit = 10;
                if (isset($_GET["page"])) {
                    $page = $_GET["page"];
                } else {
                    $page = 1;
                };
                $start_from = ($page - 1) * $limit;
                $count = 1;

                $sql = "SELECT * FROM draws ORDER BY draw_date ASC LIMIT $start_from, $limit";
                $rs_result = mysqli_query($con, $sql);
                ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Draw Date</th>
                    </tr>
                    <thead>
                    <tbody>
                    <?php
                    while ($row = mysqli_fetch_assoc($rs_result)) {
                        $id = $row['id'];
                        ?>
                        <tr>
                            <td><?php echo $count++; ?></td>
                            <td>
                                <a href="winners.php?draw_date=<?php echo $row['draw_date']; ?>"><?php echo $row["draw_date"]; ?> Click to view winners of this draw.</a>
                            </td>
                        </tr>
                        <?php
                    };
                    ?>
                    </tbody>
                </table>
                <?php
                $sql = "SELECT COUNT(id) FROM participants";
                $rs_result = mysqli_query($con, $sql);
                $row = mysqli_fetch_row($rs_result);
                $total_records = $row[0];
                $total_pages = ceil($total_records / $limit);
                $pagLink = "<ul class='pagination'>";
                for ($i = 1; $i <= $total_pages; $i++) {
                    $pagLink .= "<li><a href='results.php?page=" . $i . "'>" . $i . "</a></li>";
                };
                echo $pagLink . "</ul>";
                ?>
            </div>
            <div class="col-md-6 about-grid animated wow slideInRight" data-wow-delay=".5s">
                <h3>Our Products</h3>
                <div class="about-gd">
                    <div class="about-gd-left">
                        <h4>Mazowe Orange Crush -</h4>
                    </div>
                    <div class="about-gd-right">
                        <p>Mazoe Orange Crush is a homegrown cordial that has been trusted by generations of mothers to
                            give real refreshment for their families. Every bottle of Mazoe Orange Crush is packed with
                            50% orange juice to give it that authentic, tangy orange taste that only Mazoe is renowned
                            for. It is great as a mixer for cocktails, fruit juice punches and an ingredient for fruit
                            smoothies.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="about-gd">
                    <div class="about-gd-left">
                        <h4>Mazoe Syrups -</h4>
                    </div>
                    <div class="about-gd-right">
                        <p>Mazoe Syrups are a rainbow range of flavoured cordials available in 1 and 2 litre packs for
                            tasty refreshment. They are available in Blackberry, Cream Soda, Raspberry and Peach
                            flavours. They are great for serving with or without meals, at home and during social
                            gatherings. With Mazoe Syrups, a little goes a long way so you get more refreshment at an
                            affordable price which is good value for money.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //about -->
<!-- events -->

<!-- //events -->
<!-- history -->

<!-- //history -->
<!-- footer -->

<div class="footer-copy">
    <div class="container">
        <div class="footer-left animated wow slideInUp" data-wow-delay=".5s">
            <p>© 2016 Schweppes. All rights reserved | Powered by <a href="http://www.rubiem.com" target="_blank">Rubiem
                    Innovations</a>
            </p>
        </div>
        <div class="footer-right animated wow slideInUp" data-wow-delay=".7s">
            <ul>
                <li><a href="#" class="p"> </a></li>
                <li><a href="#" class="facebook"> </a></li>
                <li><a href="#" class="in"> </a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
</body>
</html>