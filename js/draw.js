$(function() {

    var tl = new TimelineLite();
    tl
        //.from("h1", 0.5, {scale: 0.5, autoAlpha: 0})
        //.from("#logo", 0.5, {scale: 0.5, autoAlpha: 01})
        .from("#spinner", 0.5, {scale: 0.5, autoAlpha: 0})
        .staggerFrom(".btn", 2, {scale: 0.5, opacity: 0, ease: Elastic.easeOut, force3D: true}, 0.2);

    var spinner = TweenLite.to("#spinner", 7, {
        rotation:"3600_cw", 
        ease:Back.easeOut.config(1.7),  
        overwrite:"auto",
        paused: "true",
        onStart: function() {
            $('#winner_number').empty();
        },
        onComplete: function() {
            $('#winner_number').append( localStorage.bulk == "true" ? '<a type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">List Winners</a>' : localStorage.winner );
            var tl = new TimelineLite();
            tl.staggerFrom("#winner_number", 0.8, {scale:0, autoAlpha:0}, 1);
        }
    });

    $('.btn').click(function() {
        spinner.play();
        spinner.time(0);
    });


});