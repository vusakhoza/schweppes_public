angular.module('schweppes', [])
.controller('controller', function($scope, $rootScope, service) {

    var tl = new TimelineLite();
    tl
        //.from("h1", 0.5, {scale: 0.5, autoAlpha: 0})
        //.from("#logo", 0.5, {scale: 0.5, autoAlpha: 01})
        .from("#spinner", 0.5, {scale: 0.5, autoAlpha: 0})
        .staggerFrom(".btn", 2, {scale: 0.5, opacity: 0, ease: Elastic.easeOut, force3D: true}, 0.2);

    var spinner = TweenLite.to("#spinner", 7, {
        rotation:"3600_cw", 
        ease:Back.easeOut.config(1.7),  
        overwrite:"auto",
        paused: "true",
        onStart: function() {
            $('#winner_number').empty();
        },
        onComplete: function() {
            $('#winner_number').append( localStorage.bulk == "true" ? '<a type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal">List Winners</a>' : localStorage.winner );
            var tl = new TimelineLite();
            tl.staggerFrom("#winner_number", 0.8, {scale:0, autoAlpha:0}, 1);
        }
    });

    $.ajax({
        type: 'GET',
        url: '../winners.json',
        success: function (response) {
            //console.log(typeof response);            
            localStorage.bulk = false;
            var arr = JSON.parse(response);
            //var arr = response;
            var i;
            $scope.prize500 = [];
            $scope.prize250 = [];
            $scope.prize100 = [];
            $scope.prize50 = [];
            $scope.prize10 = [];
            $scope.prize5 = [];
            $scope.prize2 = [];

            $scope.$date = arr[0].draw.name;

            for(i = 0; i < arr.length; i++) {
                //console.log(i)
                if(arr[i].prizevalue == 500){
                    $scope.prize500.push(arr[i].cellphone);
                } else if(arr[i].prizevalue == 250){
                    $scope.prize250.push(arr[i].cellphone);
                } else if(arr[i].prizevalue == 100){
                    $scope.prize100.push(arr[i].cellphone);
                } else if(arr[i].prizevalue == 50){
                    $scope.prize50.push(arr[i].cellphone);
                } else if(arr[i].prizevalue == 10){
                    $scope.prize10.push(arr[i].cellphone);
                } else if(arr[i].prizevalue == 5){
                    $scope.prize5.push(arr[i].cellphone);
                } else if(arr[i].prizevalue == 2.50){
                    $scope.prize2.push(arr[i].cellphone);
                } 
            }

            $rootScope.$state = 1;
            service.checkstate($rootScope.$state, $scope);
            //console.log($rootScope.$winners_left);
            
            $scope.draw = function() {
                localStorage.winner = service.draw($rootScope.$winners, $scope, localStorage.bulk);
                spinner.play();
                spinner.time(0);
            }
        }
    });

})

.factory('service', function($http, $rootScope, $timeout, $location){

    return {
        checkstate: function(state, $scope) {
            var self = this;
            $timeout(function() {
                switch(state) {
                    case 1:
                        $rootScope.$winners_left = $scope.prize100.length;
                        $rootScope.$winners = $scope.prize100;
                        $rootScope.$prize = "$100 Prize"
                        break;
                    case 2:
                        $rootScope.$winners_left = $scope.prize50.length;
                        $rootScope.$winners = $scope.prize50;
                        $rootScope.$prize = "$50 Prize"
                        localStorage.bulk = true;
                        break;
                    case 3:
                        $rootScope.$winners_left = $scope.prize10.length;
                        $rootScope.$winners = $scope.prize10;
                        $rootScope.$prize = "$10 Airtime Prize"
                        localStorage.bulk = true;
                        break;
                    case 4:
                        $rootScope.$winners_left = $scope.prize5.length;
                        $rootScope.$winners = $scope.prize5;
                        $rootScope.$prize = "$5 Airtime Prize"
                        localStorage.bulk = true;
                        break;
                    case 5:
                        $rootScope.$winners_left = $scope.prize2.length;
                        $rootScope.$winners = $scope.prize2;
                        $rootScope.$prize = "$2.50 Airtime Prize"
                        localStorage.bulk = true;
                        break;
                }
            });
            // $rootScope.$digest();//refresh the angular scope
        },
        draw: function(array, $scope, bool) {
            var self = this;
            localStorage.bulk = false;

            if( bool == "true" ) $rootScope.$winners_left = 0;
            console.log($rootScope.$winners_left);

            var w = array.pop();
            if( $rootScope.$winners_left == 0 ) {
                $rootScope.$state++;
                self.checkstate($rootScope.$state, $scope);
                console.log($rootScope.$winners.length+":"+$rootScope.$winners_left);
            }

            $rootScope.$winners_left = array.length >= 0 ? array.length : $rootScope.$winners_left;
            //console.log($rootScope.$state);
            return w;
        },
    }

});